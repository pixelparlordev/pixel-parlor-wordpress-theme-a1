
# Pixel Parlor Wordpress Theme A1
- [Folder: PXP-Theme] > [Folder: Divi] | [Folder: pxp-child-theme]

• Child Theme of Divi

• Basic CSS (No syntactic sugar or pre-processors for this version =))

• Basic Javascript (No syntactic sugar or pre-processors for this version =), files live in scripts/script.js)

• Starter Files (The code files must be replenished/replaced every three months for new builds)

* style.css
* screenshot.jpg
* README.md
* 404.php
* footer.php
* functions.php
* header.php
* home-page.php
* index.php
* page-template-blank.php
* page.php
* sidebar.php
* single-et_pb_layout.php
* single.php
* x-footer.php
* .gitignore
* LICENSE

## CSS

If you don't have any experience working with preprocessors, this is the best child theme for your purposes.

#### Installation

The first step to getting start with building a child theme is cloning this repo. You can download the PXP-Theme folder and drop the contents into your `wp-content/themes` folder.

1. Add Divi Theme and Pixel Parlor Child Theme to your `wp-content/themes` folder instead your WordPress directory.

#### Development

Now that you have the theme installed, you can start development.

1. If you haven't already, navigate to your `pixel-parlor-theme-a`.
2. Open up `style.css` in your favorite editor and code away!
3. Copy over any other files you might need from the Divi theme included.

## Copyright & License
Copyright 2018 Pixel Parlor. Code released under the [GNU license](https://www.pixelparlor.com).