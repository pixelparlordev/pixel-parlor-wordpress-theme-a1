<?php
//
// Recommended way to include parent theme styles.
//  (Please see http://codex.wordpress.org/Child_Themes#How_to_Create_a_Child_Theme)
//
add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );
function theme_enqueue_styles() {
    wp_enqueue_style( 'divi', get_template_directory_uri() . '/style.css' );
    //wp_enqueue_script( 'divi', plugin_dir_url( __FILE__ ) . 'js/scripts.js', array( 'jquery', 'divi-custom-script' ), '0.1.2', true );
  wp_enqueue_script( 'app', get_bloginfo( 'stylesheet_directory' ) . '/js/scripts.js', array( 'jquery' ), '1.0.0' );
}

//
// Pixel Parlor Custom Functions
//

// Removes et_add_viewport_meta from the wp_head phase

function remove_divi_actions() {
remove_action( ‘wp_head’, ‘et_add_viewport_meta’ );
}

// Call ‘remove_divi_actions’ during WP initialization

add_action(‘init’,’remove_divi_actions’);
function et_add_viewport_meta_2(){
echo ”;
}
add_action( ‘wp_head’, ‘et_add_viewport_meta_2’ );


// Allow SVG uploads

function allow_svgimg_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'allow_svgimg_types');


// Hide Wordpress Loggin Errors

function no_wordpress_errors(){
  return 'Something is wrong!';
}
add_filter( 'login_errors', 'no_wordpress_errors' );


add_action( 'wp_enqueue_scripts', 'prefix_enqueue_awesome' );
/**
* Register and load font awesome CSS files using a CDN.
*
* @link   http://www.bootstrapcdn.com/#fontawesome
* @author FAT Media
*/
function prefix_enqueue_awesome() {
wp_enqueue_style( 'prefix-font-awesome', '//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css', array(), '4.0.3' );
}


// Register Additional Navigation Menus

function wpb_custom_new_menu() {
  register_nav_menus(
    array(
      'my-custom-menu' => __( 'My Custom Menu' ),
      'extra-menu' => __( 'Extra Menu' )
    )
  );
}
add_action( 'init', 'wpb_custom_new_menu' );


// Custom WordPress Development Dashboard Widget

add_action('wp_dashboard_setup', 'my_custom_dashboard_widgets');

function my_custom_dashboard_widgets() {
global $wp_meta_boxes;

wp_add_dashboard_widget('custom_help_widget', 'Theme Support', 'custom_dashboard_help');
}

function custom_dashboard_help() {
echo '<p>Welcome to Pixel Parlor Custom Theme ! Need help? Contact the developer <a href="mailto:dev@pixelparlor.com">here</a>. For More info visit: <a href="http://www.pixelparlor.com/" target="_blank">pixelparlor.com</a></p>';
}


// Add a Custom Dashboard Logo

function dev_custom_logo() {
echo '
<style type="text/css">
#wpadminbar #wp-admin-bar-wp-logo > .ab-item .ab-icon:before {
background-image: url(' . get_bloginfo('stylesheet_directory') . '/images/pxp_fvcn.png) !important;
background-position: 0 0;
background-size: cover;
background-repeat: no-repeat;
color:rgba(0, 0, 0, 0);
}
#wpadminbar #wp-admin-bar-wp-logo.hover > .ab-item .ab-icon {
background-position: 0 0;
}
</style>
';
}

//hook into the administrative header output

add_action('wp_before_admin_bar_render', 'dev_custom_logo');


// Change Footer Branding In WordPress Dash

function remove_footer_admin () {

echo 'Website Design & Develpment by <a href="http://www.pixelparlor.com/" target="_blank">Pixel Parlor</a></p>';

}

add_filter('admin_footer_text', 'remove_footer_admin');


// Branded Gravatars for Comments

// customize default gravatars
function custom_gravatars($avatar_defaults) {

  // change the default gravatar

  $customGravatar1 = get_bloginfo('template_directory').'/images/gw-gravatar.png';
  $avatar_defaults[$customGravatar1] = 'Default';

  // add a custom user gravatar

  $customGravatar2 = get_bloginfo('template_directory').'/images/gw-gravatar.png';
  $avatar_defaults[$customGravatar2] = 'Custom Gravatar';

  // add another custom gravatar

  $customGravatar3 = get_bloginfo('template_directory').'/images/gw-gravatar.png';
  $avatar_defaults[$customGravatar3] = 'Custom gravatar';

  return $avatar_defaults;
}
add_filter('avatar_defaults', 'custom_gravatars');


// Divi Custom Footer Editor

function ds_footer_links_editor($wp_customize) {

 $wp_customize->add_panel( 'footer_links_option', array(
 'priority' => 30,
 'capability' => 'edit_theme_options',
 'title' => __('Edit Footer Links', footer_links_title),
 'description' => __('Customize the login of your website.', footer_links_title),
 ));

 $wp_customize->add_section('ds_footer_links_section', array(
 'priority' => 5,
 'title' => __('Footer Links Editor', footer_links_title),
 'panel' => 'footer_links_option',
 ));

 // Before Link One

 $wp_customize->add_setting('ds_footer_links_before_link_one', array(
 'default' => 'Designed By',
 'type' => 'option',
 'capability' => 'edit_theme_options',
 ));

 $wp_customize->add_control('ds_footer_links_before_link_one', array(
 'label' => __('Text Before First Link', footer_links_title),
 'section' => 'ds_footer_links_section',
 'type' => 'option',
 'priority' => 5,
 'settings' => 'ds_footer_links_before_link_one'
 ));

 // Link One

 $wp_customize->add_setting('ds_footer_links_link_one', array(
 'default' => 'Elegant Themes',
 'type' => 'option',
 'capability' => 'edit_theme_options',
 ));

 $wp_customize->add_control('ds_footer_links_link_one', array(
 'label' => __('First Link Text', footer_links_title),
 'section' => 'ds_footer_links_section',
 'type' => 'option',
 'priority' => 10,
 'settings' => 'ds_footer_links_link_one'
 ));

 // Link One URL

 $wp_customize->add_setting('ds_footer_link_one_url', array(
 'default' => '#',
 'type' => 'option',
 'capability' => 'edit_theme_options',
 ));

 $wp_customize->add_control('ds_footer_link_one_url', array(
 'label' => __('First Link URL', footer_links_title),
 'section' => 'ds_footer_links_section',
 'type' => 'option',
 'priority' => 15,
 'settings' => 'ds_footer_link_one_url'
 ));

// Before Link Two

 $wp_customize->add_setting('ds_footer_links_before_link_two', array(
 'default' => 'Powered By',
 'type' => 'option',
 'capability' => 'edit_theme_options',
 ));

 $wp_customize->add_control('ds_footer_links_before_link_two', array(
 'label' => __('Text Before Second Link', footer_links_title),
 'section' => 'ds_footer_links_section',
 'type' => 'option',
 'priority' => 20,
 'settings' => 'ds_footer_links_before_link_two'
 ));

 // Link Two

 $wp_customize->add_setting('ds_footer_links_link_two', array(
 'default' => 'WordPress',
 'type' => 'option',
 'capability' => 'edit_theme_options',
 ));

 $wp_customize->add_control('ds_footer_links_link_two', array(
 'label' => __('Second Link Text', footer_links_title),
 'section' => 'ds_footer_links_section',
 'type' => 'option',
 'priority' => 25,
 'settings' => 'ds_footer_links_link_two'
 ));

 // Link Two URL

 $wp_customize->add_setting('ds_footer_link_two_url', array(
 'default' => '###',
 'type' => 'option',
 'capability' => 'edit_theme_options',
 ));

 $wp_customize->add_control('ds_footer_link_two_url', array(
 'label' => __('Second Link URL', footer_links_title),
 'section' => 'ds_footer_links_section',
 'type' => 'option',
 'priority' => 30,
 'settings' => 'ds_footer_link_two_url'
 ));

 // Footer Divider

 $wp_customize->add_setting('ds_footer_link_divider', array(
 'default' => '|',
 'type' => 'option',
 'capability' => 'edit_theme_options',
 ));

 $wp_customize->add_control('ds_footer_link_divider', array(
 'label' => __('Footer Link Divider', footer_links_title),
 'section' => 'ds_footer_links_section',
 'type' => 'option',
 'priority' => 35,
 'settings' => 'ds_footer_link_divider'
 ));
}

add_action('customize_register', 'ds_footer_links_editor');

function ds_new_bottom_footer() {

$footer_one = get_option('ds_footer_links_before_link_one','Designed By');
$footer_two = get_option('ds_footer_links_link_one','Elegant Themes');
$footer_link_one = get_option('ds_footer_link_one_url','http://www.elegantthemes.com/');
$footer_three = get_option('ds_footer_links_before_link_two','Powered By');
$footer_four = get_option('ds_footer_links_link_two', 'WordPress');
$footer_link_two = get_option('ds_footer_link_two_url', 'https://wordpress.org/');
$footer_divider = get_option('ds_footer_link_divider','|');

?>

<script type="text/javascript">
jQuery(document).ready(function(){
jQuery("#footer-info").text(' ');
jQuery('<p id="footer-info"><?php echo $footer_one; ?> <a href="<?php echo $footer_link_one; ?>"><?php echo $footer_two; ?></a> <?php echo $footer_divider; ?> <?php echo $footer_three; ?> <a href="<?php echo $footer_link_two; ?>"><?php echo $footer_four; ?></a></p>').insertAfter("#footer-info");
});
</script>

<?php
}

add_action( 'wp_head', 'ds_new_bottom_footer' );


// Divi Function for Shortcode to show the module within another module

function showmodule_shortcode($moduleid) {
  extract(shortcode_atts(array('id' =>'*'),$moduleid));
  return do_shortcode('[et_pb_section global_module="'.$id.'"][/et_pb_section]');
}
add_shortcode('showmodule', 'showmodule_shortcode');


// Remove Empy P tags in shortcodes

function shortcode_empty_paragraph_fix( $content ) {

    $array = array (
        '<p>[' => '[',
        ']</p>' => ']',
        ']<br />' => ']'
    );

    $content = strtr( $content, $array );

    return $content;
}

add_filter( 'the_content', 'shortcode_empty_paragraph_fix' );