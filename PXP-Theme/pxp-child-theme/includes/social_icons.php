<ul class="et-social-icons">


<li class="fa-social-icon">
	<a target="_blank" href="#">
	<span><i class="et-social-icon fa fa-github-alt"></i></span></a></span>
	</a>
</li>

<li class="et-social-icon et-social-linkedin">
	<a target="_blank" href="#">
	<span><?php esc_html_e( 'Linkedin', 'Divi' ); ?></span>
	</a>
</li>

<?php if ( 'on' === et_get_option( 'divi_show_twitter_icon', 'on' ) ) : ?>
	<li class="et-social-icon et-social-twitter">
		<a href="<?php echo esc_url( et_get_option( 'divi_twitter_url', '#' ) ); ?>" class="icon">
			<span><?php esc_html_e( 'Twitter', 'Divi' ); ?></span>
		</a>
	</li>
<?php endif; ?>

<?php if ( 'on' === et_get_option( 'divi_show_google_icon', 'on' ) ) : ?>
	<li class="et-social-icon et-social-google-plus">
		<a href="<?php echo esc_url( et_get_option( 'divi_google_url', '#' ) ); ?>" class="icon">
			<span><?php esc_html_e( 'Google', 'Divi' ); ?></span>
		</a>
	</li>
<?php endif; ?>

<li class="et-social-icon et-social-instagram">
	<a target="_blank" href=#">
	<span><?php esc_html_e( 'Instagram', 'Divi' ); ?></span>
	</a>
</li>

<li class="et-social-icon et-social-pinterest">
	<a target="_blank" href="https://www.pinterest.com/robwcreate/">
	<span><?php esc_html_e( 'Pinterest', 'Divi' ); ?></span>
	</a>
</li>

<?php if ( 'on' === et_get_option( 'divi_show_rss_icon', 'on' ) ) : ?>
<?php
	$et_rss_url = '' !== et_get_option( 'divi_rss_url' )
		? et_get_option( 'divi_rss_url' )
		: get_bloginfo( 'rss2_url' );
?>
	<li class="et-social-icon et-social-rss">
		<a href="<?php echo esc_url( $et_rss_url ); ?>" class="icon">
			<span><?php esc_html_e( 'RSS', 'Divi' ); ?></span>
		</a>
	</li>
<?php endif; ?>

</ul>
